package br.ufrn.imd.webservicesexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import domain.Fornecedor;

public class MainActivity extends AppCompatActivity {

    private List<Fornecedor> fornecedores;
    private EditText ufText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ufText = (EditText) findViewById(R.id.ufText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void requestFornecedores() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://compras.dados.gov.br/" +
                "fornecedores/v1/fornecedores.json?uf="
                + ufText.getText().toString();
        StringRequest stringRequest = new StringRequest
                (Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        fornecedores = gerarFornecedoresFromJson(response);
                        progressBar.setVisibility(ProgressBar.GONE);
                        Intent i  = new Intent(MainActivity.this, ListActivity.class);
                        i.putExtra("fornecedores", (Serializable) fornecedores);
                        startActivity(i);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        queue.add(stringRequest);
    }

    private List<Fornecedor> gerarFornecedoresFromJson(String dados) {
        List<Fornecedor> resultado = new ArrayList<Fornecedor>();
        try {
            JSONObject jsonObject = new JSONObject(dados);
            JSONObject emb = jsonObject.getJSONObject("_embedded");
            JSONArray ja = emb.getJSONArray("fornecedores");
            for(int i = 0; i<= ja.length(); i++) {
                JSONObject jo = ja.getJSONObject(i);
                Fornecedor f = new Fornecedor();
                f.setNome(jo.getString("nome"));
                try {
                    f.setCnpj(jo.getString("cnpj"));
                }catch (JSONException e) {
                    f.setCnpj(jo.getString("cpf"));
                }
                resultado.add(f);
            }
            return resultado;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public void buscar(View view) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        requestFornecedores();
    }
}
