package br.ufrn.imd.webservicesexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

import adapters.FornecedorListaAdapter;
import domain.Fornecedor;

public class ListActivity extends AppCompatActivity {

    private ListView fornecedoresListView;
    private FornecedorListaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        List<Fornecedor> fornecedores = (List<Fornecedor>)
                getIntent().getSerializableExtra("fornecedores");

        fornecedoresListView = (ListView) findViewById(R.id.listfornecedores);
        adapter = new FornecedorListaAdapter(this,
                R.layout.list_fornecedores, fornecedores);

        fornecedoresListView.setAdapter(adapter);
    }
}
